package com.manaknightdigital.smsapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Prefs {
    private static final String KEY_PREFERENCE_NAME = "key_pref";
    private static String TIME = "time";
    private static String API_ENDPOINT = "api";
    private static String TEXT_LIMIT = "limit";
    private static String TEXT_COUNT = "count";

    public static void setTime(Context context, double time) {
        SharedPreferences.Editor editor = context.getSharedPreferences(KEY_PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        editor.putFloat(TIME, (float) time);
        editor.apply();
    }

    public static double getTime(Context context) {
        return context.getSharedPreferences(KEY_PREFERENCE_NAME, Context.MODE_PRIVATE).getFloat(TIME, 0);
    }

    public static void setLimit(Context context, int limit) {
        SharedPreferences.Editor editor = context.getSharedPreferences(KEY_PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        editor.putInt(TEXT_LIMIT, limit);
        editor.apply();
    }

    public static int getLimit(Context context) {
        return context.getSharedPreferences(KEY_PREFERENCE_NAME, Context.MODE_PRIVATE).getInt(TEXT_LIMIT, 0);
    }

    public static void setCount(Context context, int count) {
        SharedPreferences.Editor editor = context.getSharedPreferences(KEY_PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        editor.putInt(TEXT_COUNT, count);
        editor.apply();
    }

    public static int getCount(Context context) {
        return context.getSharedPreferences(KEY_PREFERENCE_NAME, Context.MODE_PRIVATE).getInt(TEXT_COUNT, 0);
    }

    public static void setApiName(Context context, String apiName) {
        SharedPreferences.Editor editor = context.getSharedPreferences(KEY_PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(API_ENDPOINT, apiName);
        editor.apply();
    }
    public static String getApiName(Context context) {
        return context.getSharedPreferences(KEY_PREFERENCE_NAME, Context.MODE_PRIVATE).getString(API_ENDPOINT, "");
    }

    public  static void clear(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(KEY_PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();
    }
}

package com.manaknightdigital.smsapp.models;

import androidx.annotation.NonNull;

public class PollingTime {
    private String displayName;
    private double time;

    public PollingTime(String displayName, double time) {
        this.displayName = displayName;
        this.time = time;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    @NonNull
    @Override
    public String toString() {
        return displayName;
    }
}

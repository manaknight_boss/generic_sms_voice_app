package com.manaknightdigital.smsapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SmsCall implements Serializable {
    private String phone;
    private String type;
    private int timeout;
    private String message;
    @SerializedName("save_api")
    private String saveEndpoint; //https://avanza360.app/v1/api/smscall/call/save/11;
    @SerializedName("save_api_no_answer")
    private String saveEndpointNoAnswer;
    @SerializedName("save_api_fail")
    private String saveEndpointFail;

    public static final String TYPE_SMS = "sms";
    public static final String TYPE_CALL = "call";

    public static final String SENT = "SMS_SENT";
    public static final String DELIVERED = "SMS_DELIVERED";

    public static final String EXTRA_SUCCESS_API = "extra_success";
    public static final String EXTRA_FAIL_API = "extra_fail";

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSaveEndpoint() {
        return saveEndpoint;
    }

    public void setSaveEndpoint(String saveEndpoint) {
        this.saveEndpoint = saveEndpoint;
    }

    public String getSaveEndpointNoAnswer() {
        return saveEndpointNoAnswer;
    }

    public void setSaveEndpointNoAnswer(String saveEndpointNoAnswer) {
        this.saveEndpointNoAnswer = saveEndpointNoAnswer;
    }

    public String getSaveEndpointFail() {
        return saveEndpointFail;
    }

    public void setSaveEndpointFail(String saveEndpointFail) {
        this.saveEndpointFail = saveEndpointFail;
    }
}

package com.manaknightdigital.smsapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.manaknightdigital.smsapp.R;
import com.manaknightdigital.smsapp.controllers.DeliveryReportReceiver;
import com.manaknightdigital.smsapp.controllers.MakeCallOrSmsTask;
import com.manaknightdigital.smsapp.utils.Prefs;
import com.manaknightdigital.smsapp.databinding.ActivityMainBinding;
import com.manaknightdigital.smsapp.interfaces.AppEvents;
import com.manaknightdigital.smsapp.models.PollingTime;
import com.manaknightdigital.smsapp.models.SmsCall;
import com.manaknightdigital.smsapp.network.APIManager;
import com.manaknightdigital.smsapp.utils.Utils;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements AppEvents {
    private ArrayList<PollingTime> mList = new ArrayList<>();
    private ActivityMainBinding mBinding;
    public Context mContext;
    private static final String TAG = "smscall";
    private RxPermissions rxPermissions;
    private DeliveryReportReceiver receiver;
    private boolean isRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mBinding.setEvents(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        init();
    }

    private void init() {
        rxPermissions = new RxPermissions(this);
        populateTimings();
        setSpinnerAdapter();
        setData();
        enableButtons();
        askPermissions();
        refreshCount();
    }

    @Override
    protected void onStart() {
        super.onStart();
        receiver = new DeliveryReportReceiver();
        registerReceiver(receiver, new IntentFilter(SmsCall.DELIVERED));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    public void askPermissions() {
        if (!doesUserHavePermission()) {
            rxPermissions.request(Manifest.permission.SEND_SMS)
                    .subscribe(permission->{
                        if (doesUserHavePermission()) {

                        } else {

                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                                boolean showRationale = shouldShowRequestPermissionRationale( Manifest.permission.SEND_SMS);
                                if (!showRationale) {
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivity(intent);
                                } else {
                                    askPermissions();
                                }
                            }
                        }
                    });
        }
    }

    public boolean doesUserHavePermission() {
        int result = this.checkCallingOrSelfPermission(Manifest.permission.SEND_SMS);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void populateTimings() {
        mList.add(new PollingTime("10 seconds", 10));
        mList.add(new PollingTime("20 seconds", 20));
        mList.add(new PollingTime("30 seconds", 30));
        mList.add(new PollingTime("40 seconds", 40));
        mList.add(new PollingTime("50 seconds", 50));
        mList.add(new PollingTime("1 minute", 60));
        mList.add(new PollingTime("2 minutes", 120));
        mList.add(new PollingTime("3 minutes", 180));
        mList.add(new PollingTime("4 minutes", 240));
        mList.add(new PollingTime("5 minutes", 300));
        mList.add(new PollingTime("6 minutes", 360));
        mList.add(new PollingTime("7 minutes", 420));
        mList.add(new PollingTime("8 minutes", 480));
        mList.add(new PollingTime("9 minutes", 540));
        mList.add(new PollingTime("10 minutes", 600));
    }

    private void setSpinnerAdapter() {
        ArrayAdapter<PollingTime> adapter =
                new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_dropdown_item, mList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBinding.spinner.setAdapter(adapter);
    }

    public void setData() {
        mBinding.apiEndPoint.setText(Prefs.getApiName(mContext));
        int limit = Prefs.getLimit(mContext);
        mBinding.textLimit.setText(""+(limit == 0 ? "" : limit));
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).getTime() == Prefs.getTime(mContext)) {
                mBinding.spinner.setSelection(i);
                break;
            }
        }
    }

    @Override
    public void onSaveClick(View view) {
        String apiEndPoint = Objects.requireNonNull(mBinding.apiEndPoint.getText()).toString().trim();
        if (!TextUtils.isEmpty(apiEndPoint)) {
            Prefs.setTime(mContext, mList.get(mBinding.spinner.getSelectedItemPosition()).getTime());
            Prefs.setApiName(mContext, apiEndPoint);
            Toast.makeText(mContext, getString(R.string.data_saved), Toast.LENGTH_SHORT).show();
        } else {
            mBinding.apiEndPoint.setError(getString(R.string.empty_error));
        }
    }

    @Override
    public void onStartClick(View view) {
        String api = Prefs.getApiName(mContext);
        if (TextUtils.isEmpty(api)) {
            Toast.makeText(mContext, getString(R.string.no_api_error), Toast.LENGTH_SHORT).show();
            return;
        }
        isRunning = true;
        getContacts();
        enableButtons();
    }

    public void getContacts() {
        if(Prefs.getLimit(mContext) != 0 && Prefs.getCount(mContext) >= Prefs.getLimit(mContext)) {
            handleApiError(getString(R.string.text_limit_reached));
        }
        if(isRunning) {
            String api = Prefs.getApiName(mContext);
            if (!api.startsWith("http")) {
                api = "http://" + api;
            }
            APIManager.instance().getData(api).enqueue(new Callback<List<SmsCall>>() {
                @Override
                public void onResponse(@NonNull Call<List<SmsCall>> call, @NonNull Response<List<SmsCall>> response) {
                    if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
                        List<SmsCall> list = response.body();
                        new MakeCallOrSmsTask(mContext, list, MainActivity.this).execute();
                    } else {
                        handleApiError(getString(R.string.bad_response));
                    }
                }

                @Override
                public void onFailure(@NonNull Call<List<SmsCall>> call, @NonNull Throwable t) {
                    handleApiError(getString(R.string.bad_response));
                }
            });
        }
    }

    @Override
    public void setTextLimit(View view) {
        int limit = Integer.parseInt(mBinding.textLimit.getText().toString().trim());
        Prefs.setLimit(mContext, limit);
        refreshCount();
        Utils.hideKeyboard(this);
    }

    @Override
    public void resetCount(View view) {
        Prefs.setCount(mContext, 0);
        refreshCount();
    }

    @Override
    public void refreshCount() {
        if(Prefs.getLimit(mContext) == 0) {
            mBinding.textCount.setText(getString(R.string.no_limit));
            return;
        }
        mBinding.textCount.setText(getString(R.string.text_counter, Prefs.getCount(mContext), Prefs.getLimit(mContext)));
    }

    private void handleApiError(String error) {
        isRunning = false;
        enableButtons();
//        Toast.makeText(mContext, getString(R.string.bad_response), Toast.LENGTH_SHORT).show();
        MediaPlayer mediaPlayer = MediaPlayer.create(mContext, R.raw.tune);
        mediaPlayer.start();
        final Snackbar snackbar = Snackbar.make(mBinding.container, error, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(getString(R.string.stop), (v) -> {
                    mediaPlayer.stop();
                    snackbar.dismiss();
                });
        snackbar.show();
    }

    @Override
    public void onStopClick(View view) {
        isRunning = false;
        enableButtons();
    }

    private void enableButtons() {
        mBinding.start.setEnabled(!isRunning);
        mBinding.stop.setEnabled(isRunning);
        mBinding.save.setEnabled(!isRunning);
        mBinding.resetButton.setEnabled(!isRunning);
        mBinding.setLimitButton.setEnabled(!isRunning);
    }

}

package com.manaknightdigital.smsapp.network;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIManager {
    private static final String API_ENDPOINT = "https://avanza360.app/";
    private static Retrofit client = null;

    public static RestService instance() {
        if (client == null) {

            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(20, TimeUnit.MINUTES)
                    .writeTimeout(20, TimeUnit.MINUTES)
                    .readTimeout(20, TimeUnit.MINUTES)
                    .build();

            client = new Retrofit.Builder()
                    .baseUrl(API_ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
                    .client(okHttpClient)
                    .build();

        }

        return client.create(RestService.class);
    }
}

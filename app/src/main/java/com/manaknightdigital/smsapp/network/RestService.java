package com.manaknightdigital.smsapp.network;

import com.manaknightdigital.smsapp.models.SmsCall;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface RestService {
    @GET
    Call<List<SmsCall>> getData(@Url String url);

    @GET
    Call<ResponseBody> save(@Url String url);
}

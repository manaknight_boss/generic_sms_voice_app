package com.manaknightdigital.smsapp.controllers;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.telephony.SmsManager;
import android.text.Html;
import android.util.Log;

import androidx.annotation.NonNull;

import com.manaknightdigital.smsapp.interfaces.AppEvents;
import com.manaknightdigital.smsapp.models.SmsCall;
import com.manaknightdigital.smsapp.network.APIManager;
import com.manaknightdigital.smsapp.utils.Prefs;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MakeCallOrSmsTask extends AsyncTask<Void, Void, Void> {

    private List<SmsCall> mList;
    private Context mContext;
    private AppEvents mCallback;
    private static final String TAG = "smscall";

    public MakeCallOrSmsTask(Context mContext, List<SmsCall> mList, AppEvents mCallback) {
        this.mList = mList;
        this.mContext = mContext;
        this.mCallback = mCallback;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            for (SmsCall contact : mList) {
                if (contact.getType().equalsIgnoreCase(SmsCall.TYPE_SMS)) {
                    if (contact.getMessage().trim().isEmpty()) {
                        //todo skip and call success api
                        continue;
                    }
                    SmsManager smsManager = SmsManager.getDefault();
                    String message = Html.fromHtml(new String(contact.getMessage().getBytes("UTF-8"))).toString();

                    ArrayList<String> parts = smsManager.divideMessage(message);
                    ArrayList<PendingIntent> deliveryIntents = new ArrayList<>();
                    ArrayList<PendingIntent> sentIntents = new ArrayList<>();

                    Intent sentIntent = new Intent(SmsCall.SENT);
                    Intent deliverIntent = new Intent(SmsCall.DELIVERED);
                    sentIntent.putExtra(SmsCall.EXTRA_SUCCESS_API, contact.getSaveEndpoint());
                    sentIntent.putExtra(SmsCall.EXTRA_FAIL_API, contact.getSaveEndpointFail());
                    deliverIntent.putExtra(SmsCall.EXTRA_SUCCESS_API, contact.getSaveEndpoint());
                    deliverIntent.putExtra(SmsCall.EXTRA_FAIL_API, contact.getSaveEndpointFail());
                    PendingIntent sentPI = PendingIntent.getBroadcast(mContext, 0, sentIntent, 0);
                    PendingIntent deliveredPI = PendingIntent.getBroadcast(mContext, 0, deliverIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    for (int i = 0; i < parts.size(); i++) {
                        sentIntents.add(sentPI);
                        deliveryIntents.add(deliveredPI);
                    }
                    String phone = contact.getPhone();
                    smsManager.sendMultipartTextMessage(phone, null, parts, sentIntents, deliveryIntents);
                    increaseCount();
                    APIManager.instance().save(contact.getSaveEndpoint()).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                            Log.d(TAG, "onResponse: Saved");
                        }

                        @Override
                        public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                            Log.d(TAG, "onResponse: Failed");
                        }
                    });
                    if(Prefs.getLimit(mContext) != 0 && Prefs.getCount(mContext) >= Prefs.getLimit(mContext)) {
                        break;
                    }
                    Thread.sleep(contact.getTimeout() * 1000);
                } else if (contact.getType().equalsIgnoreCase(SmsCall.TYPE_CALL)) {
                    //todo skip this case and pul calling logic later
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, "doInBackground: "+ex.getMessage());
        }
        return null;
    }

    private void increaseCount() {
        int count = Prefs.getCount(mContext);
        Prefs.setCount(mContext, ++count);
        mCallback.refreshCount();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        Log.d(TAG, "onPostExecute: Ended");
        new Handler().postDelayed(() -> mCallback.getContacts(), ((long)Prefs.getTime(mContext)) * 1000);
    }
}

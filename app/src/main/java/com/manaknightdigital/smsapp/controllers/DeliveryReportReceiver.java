package com.manaknightdigital.smsapp.controllers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.manaknightdigital.smsapp.models.SmsCall;

public class DeliveryReportReceiver extends BroadcastReceiver {
    private static final String TAG = "smscall";

    @Override
    public void onReceive(Context mContext, Intent intent) {
        int resultCode = getResultCode();
        String successApi = intent.getStringExtra(SmsCall.EXTRA_SUCCESS_API);
        String failApi = intent.getStringExtra(SmsCall.EXTRA_FAIL_API);
        if (resultCode == Activity.RESULT_OK) {
            // sms delivered
//            Toast.makeText(mContext, "SMS sent: " + successApi, Toast.LENGTH_LONG).show();
        } else {
            // delivery failed
//            Toast.makeText(mContext, "Error: " + failApi, Toast.LENGTH_LONG).show();
        }
    }
}

package com.manaknightdigital.smsapp.interfaces;

import android.view.View;

public interface AppEvents {
    void onSaveClick(View view);
    void onStartClick(View view);
    void onStopClick(View view);
    void getContacts();
    void setTextLimit(View view);
    void resetCount(View view);
    void refreshCount();
}
